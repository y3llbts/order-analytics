<?
    $MESS ['PM_SERVICE'] = "Рекламный сервис";
    $MESS ['PM_CAMPAIGN'] = "Рекламная кампания";
    $MESS ['PM_AD'] = "Рекламное объявление";
    $MESS ['PM_SOURCE'] = "Источник перехода";
    $MESS ['PM_KEYWORD'] = "Ключевое слово";
    $MESS ['PM_SEARCH_SYSTEM'] = "Поисковая система";
    $MESS ['PM_SEARCH_QUERY'] = "Поисковый запрос";
    $MESS ['PM_REGION'] = "Регион";
    $MESS ['PM_REFERER'] = "REFERER";
?>
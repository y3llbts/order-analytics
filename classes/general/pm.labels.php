<?
IncludeModuleLangFile(__FILE__);

class PromediaAnalyticsHeaders {
	
	function headerCheckValue() {
		if($arHeaderValues = self::getUtmValues()) {
			return $arHeaderValues;
		} else {
			return false;
		}
	}

	static function getUtmValues() {
		$arHeaderValues = array();
		if(strlen($_REQUEST["utm_source"]) && $_REQUEST["utm_source"] != 'null') {
			$arHeaderValues["PM_SERVICE"] = $_REQUEST["utm_source"];
		}
		if(strlen($_REQUEST["utm_medium"]) && $_REQUEST["utm_medium"] != 'null') {
			$arHeaderValues["PM_SOURCE"] = $_REQUEST["utm_medium"];
		}
		if(strlen($_REQUEST["utm_content"]) && $_REQUEST["utm_content"] != 'null') {
			$arHeaderValues["PM_AD"] = iconv("UTF-8", "windows-1251", $_REQUEST["utm_content"]);
		}
		if(strlen($_REQUEST["utm_campaign"]) && $_REQUEST["utm_campaign"] != 'null') {
			$arHeaderValues["PM_CAMPAIGN"] = $_REQUEST["utm_campaign"];
		}
		if(strlen($_REQUEST["keyword"]) && $_REQUEST["keyword"] != 'null') {
			$arHeaderValues["PM_KEYWORD"] = $_REQUEST["keyword"];
		}
		
		if(count($arHeaderValues))
			return $arHeaderValues;
		
		return false;
	}
}
?>
<?
IncludeModuleLangFile(__FILE__);

class PromediaAnalyticsTools {
	
	public function getOrder() {
		$arOrderProps = array(
			array("CODE" => "PM_SERVICE", "NAME" => GetMessage("PM_SERVICE"), "TYPE" => "TEXT", "SORT" => "100"),
			array("CODE" => "PM_CAMPAIGN", "NAME" => GetMessage("PM_CAMPAIGN"), "TYPE" => "TEXT", "SORT" => "200"),
			array("CODE" => "PM_AD", "NAME" => GetMessage("PM_AD"), "TYPE" => "TEXT", "SORT" => "300"),
			array("CODE" => "PM_SOURCE", "NAME" => GetMessage("PM_SOURCE"), "TYPE" => "TEXT", "SORT" => "400"),
			array("CODE" => "PM_KEYWORD", "NAME" => GetMessage("PM_KEYWORD"), "TYPE" => "TEXT", "SORT" => "500"),
			array("CODE" => "PM_SEARCH_SYSTEM", "NAME" => GetMessage("PM_SEARCH_SYSTEM"), "TYPE" => "TEXT", "SORT" => "600"),
			array("CODE" => "PM_SEARCH_QUERY", "NAME" => GetMessage("PM_SEARCH_QUERY"), "TYPE" => "TEXT", "SORT" => "700"),
			array("CODE" => "PM_REGION", "NAME" => GetMessage("PM_REGION"), "TYPE" => "TEXT", "SORT" => "800"),
			array("CODE" => "PM_REFERER", "NAME" => GetMessage("PM_REFERER"), "TYPE" => "TEXT", "SORT" => "900")
		);
		return $arOrderProps;
	}
	
	public function getUserData($arParams) {
		if(!is_array($arParams) || !count($arParams))
			return array();
		
		$arCheckedParams = array();
		foreach($arParams as $key=>$param){
			if(strlen($param))
				$arCheckedParams[$key] = $param;
		}
		return $arCheckedParams;
	}
}
?>
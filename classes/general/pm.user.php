<?
IncludeModuleLangFile(__FILE__);

class PromediaAnalyticsUser {
	
	public $SERVICE = false;
	public $CAMPAIGN = false;
	public $AD = false;			
	public $SOURCE = false;			
	public $KEYWORD = false;				
	public $SEARCH_SYSTEM = false;		
	public $SEARCH_QUERY = false;		
	public $REGION = false;				
	public $REFERER = false;			
	
	function __construct($arParams = array()) {
		if (count($arParams)) {
			$this->setUserParams($arParams);
		}
	}
	
	public function setUserParams($arParams) {
		if (count($arParams)) {
			$this->SERVICE = $arParams["PM_SERVICE"];
			$this->CAMPAIGN = $arParams["PM_CAMPAIGN"];
			$this->AD = $arParams["PM_AD"];
			$this->SOURCE = $arParams["PM_SOURCE"];
			$this->KEYWORD = $arParams["PM_KEYWORD"];
			$this->SEARCH_SYSTEM = $arParams["PM_SEARCH_SYSTEM"];
			$this->SEARCH_QUERY = $arParams["PM_SEARCH_QUERY"];
			$this->REGION = $arParams["PM_REGION"];
			$this->REFERER = $arParams["PM_REFERER"];
		}
	}
	
	public function getUserParams() {
		return get_object_vars($this);
	}
}
?>
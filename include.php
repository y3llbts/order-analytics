<?
require_once ($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/promedia.orderanalytics/classes/general/pm.search.php");
require_once ($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/promedia.orderanalytics/classes/general/pm.user.php");
require_once ($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/promedia.orderanalytics/classes/general/pm.labels.php");
require_once ($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/promedia.orderanalytics/classes/general/pm.tools.php");

IncludeModuleLangFile(__FILE__);

class PromediaAnalytics {
	static $USER_COOKIE_NAME = "PM_USER_COOKIE_DATA";
	static $MODULE_NAME = "promedia.orderanalytics";
	
	public function printr($var) {
		echo '<pre>';
		print_r($var);
		echo '</pre>';
	}

	public function OnPageStartHandler() {
		$arUserData = array();
		$arUserData = self::GetUserCookieData();
		$obModuleUser = new PromediaAnalyticsUser($arUserData);
		
		$referer = $_SERVER["HTTP_REFERER"];
		
		
		$arParams = array();
		if($arSearchData = PromediaAnalyticsSearch::getSearchRequest($referer)) {
			$arParams = array_merge($arParams, $arSearchData);
			$arParams["referer"] = $referer;
		}
		if($arLabelData = PromediaAnalyticsHeaders::headerCheckValue()) {
			$arParams = array_merge($arParams, $arLabelData);
			$arParams["referer"] = $referer;
		}
		$arParams = PromediaAnalyticsTools::getUserData($arParams);
		
		if(!count($arParams)) {
			if(strlen($referer)) {
				if(self::CheckReferer($referer)) {
					$arParams["referer"] = $referer;
				}
			} else {
				$arParams["referer"] = GetMessage("PM_REFERER_LEAD");
				if(count($arUserData)) {
					if(strlen($arUserData["PM_SERIVCE"])) {
						$arParams["PM_SERIVCE"] = $arUserData["PM_SERIVCE"];
					}
					if(strlen($arUserData["PM_CAMPAIGN"])) {
						$arParams["PM_CAMPAIGN"] = $arUserData["PM_CAMPAIGN"];
					}
					if(strlen($arUserData["PM_AD"])) {
						$arParams["PM_AD"] = $arUserData["PM_AD"];
					}
					if(strlen($arUserData["PM_SOURCE"])) {
						$arParams["PM_SOURCE"] = $arUserData["PM_SOURCE"];
					}
					if(strlen($arUserData["PM_KEYWORD"])) {
						$arParams["PM_KEYWORD"] = $arUserData["PM_KEYWORD"];
					}
				}
			}
		}
		
		if(count($arParams)) {
			$useHostAsReferer = COption::GetOptionString("promedia.orderanalytics", "pm_host_ref", "");
			if(
				strlen($arParams["referer"]) && 
				$arParams["referer"] !== GetMessage("PM_REFERER_LEAD") &&
				strlen($useHostAsReferer)
			) {
				$arReferer = parse_url($arParams["referer"]);
				if(strlen($arReferer["host"])) {
					$arParams["referer"] = $arReferer["host"];
				}
			}
			$arUserData = $arParams;
			$obModuleUser->setUserParams($arUserData);
			self::SetUserCookieData($arUserData);
		}
		$GLOBALS["obModuleUser"] = $obModuleUser;
	}
	
	static function CheckReferer($referer) {
		if(!strlen($referer)) {
			return false;
		}
		$arReferer = parse_url($referer);
		
		$http_host = $_SERVER["HTTP_HOST"];
		$arHost = explode(":", $http_host);
		$http_host = $arHost[0];
		
		if($arReferer["host"] !== $http_host) {
			return true;
		}
		return false;
	}
	
	static function GetUserCookieData() {
		global $APPLICATION;
		$arUserData = array();
		$PM_USER_COOKIE_DATA = $APPLICATION->get_cookie(self::$USER_COOKIE_NAME);
		if(strlen($PM_USER_COOKIE_DATA)) {
			$arUserData = unserialize($PM_USER_COOKIE_DATA);
		} else {
			$arUserData = self::GetOldUserCookieData();
		}
		return $arUserData;
	}
	
	static function GetOldUserCookieData() {
		global $APPLICATION;
		
		$arUserData = array();
		$PM_USER_COOKIE_DATA = $APPLICATION->get_cookie("PromediaAnalytics");
		if(strlen($PM_USER_COOKIE_DATA)) {
			$arOldUserData = unserialize($PM_USER_COOKIE_DATA);
			foreach($arOldUserData as $key=>$data) {
				if($key == "service_id") {
					$arUserData["PM_SERVICE"] = $data;
				} elseif($key == "campaign_id") {
					$arUserData["PM_CAMPAIGN"] = $data;
				} elseif($key == "adv_id") {
					$arUserData["PM_AD"] = $data;
				} elseif($key == "source_id") {
					$arUserData["PM_SOURCE"] = $data;
				} elseif($key == "search_system") {
					$arUserData["PM_SEARCH_SYSTEM"] = $data;
				} elseif($key == "search_query") {
					$arQueryData = explode(";", $data);
					foreach($arQueryData as $key=>$str) {
						$str = trim($str);
						if($key == 0) {			
							$arUserData["PM_SEARCH_QUERY"] = substr($str, 8);
						} elseif($key == 1) {		
							$arUserData["PM_REGION"] = substr($str, 8);
						}
					}
				}
			}
		}
		return $arUserData;
	}
	
	static function SetUserCookieData($arUserData) {
		global $APPLICATION;
		
		$expire = COption::GetOptionString(self::$MODULE_NAME, "pm_expire_cookie", 30);
		
		$strUserData = serialize($arUserData);
		$APPLICATION->set_cookie(self::$USER_COOKIE_NAME, $strUserData, time()+60*60*24*$expire);
	}
	
	public function OnOrderAddHandler($ORDER_ID, $arFields) {
		PromediaAnalytics::SetOrderProps($ORDER_ID, $arFields);	
	}
	
	public function OnOrderSaveHandler($ORDER_ID, $arFields, $arOrder, $isNew) {
		PromediaAnalytics::SetOrderProps($ORDER_ID, $arFields);	
	}
	
	public function OnSaleComponentOrderOneStepCompleteHandler($ID, $arOrder) {
		PromediaAnalytics::SetOrderProps($ID, $arOrder);
	}
	
	function SetOrderProps($ORDER_ID, $arFields) {
		if(!CModule::IncludeModule("sale"))
			return false;
		global $obModuleUser;
		
		$arOrderProps = PromediaAnalyticsTools::getOrder();
		
		foreach($arOrderProps as $arProp) {
			$arOrderPropsCodes[] = $arProp["CODE"];
		}
		$db_props = CSaleOrderProps::GetList(
			array("SORT" => "ASC"),
			array(
				"PERSON_TYPE_ID" => $arFields["PERSON_TYPE_ID"],
				"CODE" => $arOrderPropsCodes,
				"ACTIVE" => "Y"
			),
			false,
			false,
			array()
		);

		while($props = $db_props->GetNext()) {
			$arAllOrderProps[] = array(
				"ID" => $props["ID"],
				"PERSON_TYPE_ID" => $props["PERSON_TYPE_ID"],
				"CODE" => $props["CODE"],
				"NAME" => $props["NAME"],
			);
		}
		
		$arUserData = $obModuleUser->getUserParams();
		foreach($arAllOrderProps as $arProp) {
			
			$code = str_replace("PM_", "", $arProp["CODE"]);
			if(array_key_exists($code, $arUserData)) {
				$arPropFields = array(
					"ORDER_ID" => 22,
					"ORDER_PROPS_ID" => $arProp["ID"],
					"NAME" => $arProp["NAME"],
					"CODE" => $arProp["CODE"],
					"VALUE" => $arUserData[$code]
				);
				
				CSaleOrderPropsValue::Add($arPropFields);
				printr($ORDER_ID);
			}
		}
	}
}
?>